import pdb
import sys
import os

gym_path = os.path.join(os.environ['HOME'], "local","gym")
sys.path.append(gym_path)

import torch
import torch.nn as nn
import numpy as np
import gym
from collections import namedtuple
from itertools import count
import random
import math
import copy
#import matplotlib.pyplot as plt

if torch.cuda.is_available():
    device="cuda"
else:
    device="cpu"




#class ConvNet_Nips(nn.Module):
#
#    def __init__(self):
#        super(ConvNet_Nips, self).__init__()
#        self.i_h1 = nn.Sequential(nn.Conv2d(4,16,kernel_size=8, stride=4),nn.ReLU())
#        self.h1_h2 = nn.Sequential(nn.Conv2d(16,32,kernel_size=4, stride=2),nn.ReLU())
#        self.h2_h3 = nn.Sequential(nn.Linear(2816, 256), nn.ReLU())
#        self.h3_o = nn.Linear(256,4)
#    
#    def forward(self, x):
#        h1 = self.i_h1(x)
#        h2 = self.h1_h2(h1).view(x.shape[0], -1)
#        h3 = self.h2_h3(h2)
#        output = self.h3_o(h3)
#        return output

class ConvNet_Nature(nn.Module):

    def __init__(self):
        super(ConvNet_Nature, self).__init__()
        self.i_h1 = nn.Sequential(nn.Conv2d(4, 32, kernel_size=8, stride=4), nn.ReLU())
        self.h1_h2 = nn.Sequential(nn.Conv2d(32, 64, kernel_size=4, stride=2), nn.ReLU())
        self.h2_h3 = nn.Sequential(nn.Conv2d(64, 64, kernel_size=3, stride=1), nn.ReLU())
        self.h3_h4 = nn.Sequential(nn.Linear(3456, 512), nn.ReLU())
        self.h4_o = nn.Linear(512, 4)

    def forward(self, x):
        h1 = self.i_h1(x)
        h2 = self.h1_h2(h1)
        h3 = self.h2_h3(h2).view(x.shape[0], -1)
        h4 = self.h3_h4(h3)
        output = self.h4_o(h4)
        return output



    

def to_grayscale(img):
    return np.mean(img, axis=2).astype(np.uint8)

def downsample(img):
    return img[::2, ::2]

def preprocess(img):
    output = to_grayscale(downsample(img))
    output =torch.tensor(output, dtype=torch.uint8, device=device).unsqueeze(0)
    return output


Transition_observation = namedtuple('Transition_observation',
                        ('observation', 'action', 'next_observation', 'reward', 'episode', 't'))

Transition_state = namedtuple('Transition_state',
                        ('state', 'action', 'next_state', 'reward'))

class ReplayMemory(object):

    def __init__(self, capacity):
        self.capacity = capacity
        self.memory = []

    def push(self, *args):
        """Saves a transition."""
        self.memory.append(Transition_observation(*args))
        if len(self.memory)>self.capacity:
            del self.memory[0]

    def sample(self, batch_size):
        output = []
        batch_ind = random.sample(range(3, len(self.memory)), batch_size)
        for ind in batch_ind:
            transitions = self.memory[ind-3:ind+1]
            mini_batch = Transition_observation(*zip(*transitions))
            if mini_batch.next_observation[0] is None or mini_batch.next_observation[1] is None or mini_batch.next_observation[2] is None:
                continue
            state = torch.cat(mini_batch.observation, 0)
            if mini_batch.next_observation[-1] is None:
                next_state = None
            else:
                next_state = torch.cat(mini_batch.next_observation, 0)
            output.append(Transition_state(state, mini_batch.action[-1], next_state, mini_batch.reward[-1]))
        return output
    
    def __len__(self):
        return len(self.memory)
    

BATCH_SIZE = 32
GAMMA = 0.99
EPS_START = 1
EPS_END = 0.1
EPS_DECAY = 1000000
TARGET_UPDATE = 10000
MEMORY_SIZE = 900000
UPDATE_FREQUENCY = 4


def select_action(state):
    """Epsilon greedy"""
    global steps_done
    sample = random.random()
    
    # epsilon decay
    #eps_threshold = EPS_END + (EPS_START - EPS_END) * math.exp(-1*steps_done / EPS_DECAY)
    lambda_ = min([steps_done,EPS_DECAY])/EPS_DECAY
    eps_threshold = lambda_ * EPS_END + (1-lambda_)*EPS_START
    steps_done += 1
    
    if sample > eps_threshold:
        with torch.no_grad():
            # t.max(1) will return largest column value of each row.
            # second column on max result is index of where max element was
            # found, so we pick action with the larger expected reward.
            state = state.new_tensor(data=state, dtype=torch.float)
            state = (state-128)/256
            #state = torch.tensor(state, device=device, dtype=torch.float)
            action = Q_value_function_net(state).max(1)[1].view(1, 1)
            return action.new_tensor(action, dtype=torch.uint8)
    else:
        return torch.tensor([[random.randrange(4)]], device=device, dtype=torch.uint8)



env = gym.make('BreakoutDeterministic-v4')

def optimize_model():
    target_net.eval()
    Q_value_function_net.train()
    
    optimizer.zero_grad()
        
    transitions = memory.sample(BATCH_SIZE)
    batch = Transition_state(*zip(*transitions))
    
    non_final_mask = torch.tensor([s is not None for s in batch.next_state], device=device, dtype=torch.uint8)
    
    non_final_next_states = torch.stack([s for s in batch.next_state if s is not None], 0)
    non_final_next_states = non_final_next_states.new_tensor(non_final_next_states, dtype=torch.float)
    non_final_next_states = (non_final_next_states-128)/256
    
    state_batch = torch.stack(batch.state, 0)
    state_batch = state_batch.new_tensor(state_batch, dtype=torch.float)
    state_batch = (state_batch-128)/256
    
    action_batch = torch.cat(batch.action)
    action_batch = action_batch.new_tensor(action_batch, dtype=torch.int64)
    reward_batch = torch.cat(batch.reward)
    
    state_action_values = Q_value_function_net(state_batch).gather(1, action_batch.view(-1,1))
    
    # Compute V(s_{t+1}) for all next states.
    # Expected values of actions for non_final_next_states are computed based
    # on the "older" target_net; selecting their best reward with max(1)[0].
    # This is merged based on the mask, such that we'll have either the expected
    # state value or 0 in case the state was final.
    next_state_values = torch.zeros(non_final_mask.shape, device=device)    
    next_state_values[non_final_mask] = target_net(non_final_next_states).max(1)[0].detach()
    
    # Compue the expected Q values
    expected_state_action_values = (next_state_values * GAMMA) + reward_batch
    
    # Compute Loss
    loss = loss_fn(state_action_values, expected_state_action_values.view(-1,1))
    loss.backward()
    nn.utils.clip_grad_norm_(Q_value_function_net.parameters(), 10, norm_type=2)
    optimizer.step()
    return loss.item()
    


def play(episode, frames):
    """
    This function plays the game with epsilon-greedy policy, where epsilon=0.01, in order to see how things go every 500 episodes. 
    """
    Q_value_function_net.eval()
    epsilon = 0.05
    old_observation = env.reset()
    old_observation = preprocess(old_observation)
    buffer_obs = [old_observation]
    R = 0
    while t in count():
        if len(buffer_obs)==4:
            state = torch.cat(buffer_obs, 0)
            state = state.new_tensor(state, dtype=torch.float)
            state = (state-128)/256
            sample = random.random()
            if sample>epsilon:
                with torch.no_grad():
                    action = Q_value_function_net(state.unsqueeze(0)).max(1)[1].view(1,1)
            else:
                action = env.action_space.sample()
                action = torch.tensor([[action]])
        else:
            action = env.action_space.sample()
            action = torch.tensor([[action]])
        new_observation, reward, done, _ = env.step(action.cpu().item())
        
        new_observation = preprocess(new_observation)
        buffer_obs.append(new_observation)
        if len(buffer_obs)==5:
            del buffer_obs[0]
        
        R += reward
        if done:
            break
    PATH_RESULTS = os.path.join(os.environ['HOME'], 'DL-RL', 'DQN/BreakoutDeterministic-v4/trained-models')
    with open(os.path.join(PATH_RESULTS, 'log_play.txt'), 'a') as f:
        f.write('Episode:{},Frames:{},R:{},t:{}\n'.format(episode, frames, R, t))





if __name__ == '__main__':
    Q_value_function_net = ConvNet_Nature()
    Q_value_function_net.to(device) 
    target_net = ConvNet_Nature()
    target_net.to(device)
    target_net.load_state_dict(Q_value_function_net.state_dict())
    target_net.eval()
    
    optimizer = torch.optim.RMSprop(Q_value_function_net.parameters(), lr=0.00025, momentum=0.95, alpha=0, eps=0.01)
    loss_fn = nn.SmoothL1Loss()
    
    memory = ReplayMemory(MEMORY_SIZE)
    memory_start_size = 50000
    
    steps_done = 0
    frames = 0
    number_param_updates = 0

    num_episodes = 35000
    for episode in range(num_episodes):
        old_observation = env.reset()
        old_observation = preprocess(old_observation)
        done = False
        R = 0
        state = [old_observation]
        for t in count():
            frames += 1
            # action selection
            if t%4 == 0:
                # from paper: repeate action selected by the agent this many times
                Q_value_function_net.eval()
                if len(memory)>memory_start_size and len(state)==4:

                    action = select_action(torch.cat(state, 0).unsqueeze(0))
                else:
                    action = env.action_space.sample()
                    action = torch.tensor([[action]], dtype=torch.uint8, device=device)
                
            Q_value_function_net.train()
            # agent executes selected ation
            new_observation, reward, done, _ = env.step(action.item())
            R += reward
            new_observation = preprocess(new_observation)
            state.append(new_observation)
            if len(state)>4:
                del state[0]
            reward = torch.tensor([reward], device=device)
            
            # store the transition in memory
            if not done:
                memory.push(old_observation, action, new_observation, reward, episode, t)
            elif done:
                memory.push(old_observation, action, None, reward, episode, t)
            
            old_observation = new_observation

            # one step of the optimization
            if len(memory)>memory_start_size:
                lambda_ = min([steps_done,EPS_DECAY])/EPS_DECAY
                eps_threshold = lambda_ * EPS_END + (1-lambda_)*EPS_START
                PATH_RESULTS = os.path.join(os.environ['HOME'], 'DL-RL', 'DQN/BreakoutDeterministic-v4/trained-models')
                if t % UPDATE_FREQUENCY == 0:
                    loss = optimize_model()
                    number_param_updates += 1
                    if number_param_updates % TARGET_UPDATE == 0:
                        target_net.load_state_dict(copy.deepcopy(Q_value_function_net.state_dict()))
                        with open(os.path.join(PATH_RESULTS, 'log.txt'),'a') as f:
                            f.write('target network updated!!!!!\n')

                    #if t%10 == 0:
                    with open(os.path.join(PATH_RESULTS, 'log.txt'), 'a') as f:
                        f.write("Episode: {}/{}, timestep: {}, R: {}, steps_done: {}, epsilon: {:.2f}, updates: {}, loss: {:.3f}, frames: {}, len(memory): {}\n".format(episode, num_episodes, t+1, R, steps_done, eps_threshold, number_param_updates, loss, frames, len(memory)))
            
            if done:
                break
        if (episode+1) % 500 == 0:
            play(episode,frames)

        if (episode+1) % 100 == 0: 
            state = {"state_dict":Q_value_function_net.state_dict()}
            PATH_RESULTS = os.path.join(os.environ['HOME'], 'DL-RL', 'DQN/BreakoutDeterministic-v4/trained-models')
            torch.save(state, os.path.join(PATH_RESULTS, 'model_episode_'+str(episode+1)+'.pth.tar'))
            
            
        
     
