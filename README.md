# Deep Reinforcement Learning
Playing with Reinforcement Learning using Openai gym

## Algorithms implemented
- Double DQN


## Relevant links/literature
- <https://spinningup.openai.com/en/latest/>
- An introduction to Deep Reinforcement Learning: <https://arxiv.org/pdf/1811.12560.pdf>
- Reinforcement Learning - An introduction. Sutton
